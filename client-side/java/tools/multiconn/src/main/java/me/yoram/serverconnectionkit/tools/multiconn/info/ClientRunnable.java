package me.yoram.serverconnectionkit.tools.multiconn.info;

import me.yoram.serverconnectionkit.tools.multiconn.status.api.ConnectionsStatus;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.ConnectionsStatusInfo;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.IStatus;

import java.io.*;
import java.net.Socket;

/**
 * Created by yoram on 21/10/16.
 */
public class ClientRunnable implements Runnable {
    private final Socket socket;
    private final IStatus status;

    ClientRunnable(final Socket socket, final IStatus status) {
        super();

        this.socket = socket;
        this.status = status;
    }

    @Override
    public void run() {
        try {
            try {
                final InputStream in = this.socket.getInputStream();

                try {
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                    writer.write("Welcome to MultiConn v0.1a Console\n");
                    writer.write("type help for command\n");
                    writer.write("\n");
                    writer.write("> ");
                    writer.flush();

                    String line;

                    while ((line = reader.readLine()) != null) {
                        line = line.trim();

                        if (line.equals("status")) {
                            ConnectionsStatus o = status.getConnectionStatus(15);

                            writer.write("opened: " + o.getConnectionOpened() + "\n");
                            writer.write("closed: " + o.getConnectionClosed() + "\n");
                            writer.write("closed with errors: " + o.getConnectionClosedWithError() + "\n");

                            for (ConnectionsStatusInfo info: o.getStatus()) {
                                String s;

                                if (info.getSecInTheLast() == 0) {
                                    s = String.format(
                                            "number of connection received message (others): %d\n",
                                            info.getNumConnectionThatReceivedMessages());
                                } else {
                                    s = String.format(
                                            "number of connection received message in the last %d sec: %d\n",
                                            info.getSecInTheLast(),
                                            info.getNumConnectionThatReceivedMessages());
                                }

                                writer.write(s);
                            }

                        }

                        writer.write("\n> ");
                        writer.flush();
                    }
                } finally {
                    in.close();
                }
            } finally {
                socket.close();
            }
        } catch (Throwable t) {

        }
    }
}
