package me.yoram.serverconnectionkit.tools.multiconn.status.impl;

import me.yoram.serverconnectionkit.tools.multiconn.status.api.ConnectionsStatus;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.ConnectionsStatusInfo;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.IStatus;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yoram on 21/10/16.
 */
public class Status implements IStatus {
    private static class StatusInfo {
        private final String name;
        private String lastMessage = null;
        private long lastMessageDate = 0;
        private int messageReceivedCount = 0;
        private Throwable error = null;
        private boolean finished = false;

        private StatusInfo(final String name) {
            super();

            this.name = name;
        }
    }

    private final Map<String, StatusInfo> map = new ConcurrentHashMap<>();

    @Override
    public void started(String name) {
        map.put(name, new StatusInfo(name));
    }

    @Override
    public void messageReceived(String name, String message) {
        final StatusInfo info = map.get(name);

        if (info == null) {
            throw new IllegalArgumentException(String.format(name + " does not exists."));
        } else if (info.finished) {
            throw new IllegalArgumentException(String.format(name + " already terminated."));
        }

        info.lastMessage = message;
        info.lastMessageDate = System.currentTimeMillis();
        info.messageReceivedCount++;
    }

    @Override
    public void finished(String name, Throwable error) {
        final StatusInfo info = map.get(name);

        if (info == null) {
            throw new IllegalArgumentException(String.format(name + " does not exists."));
        } else if (info.finished) {
            throw new IllegalArgumentException(String.format(name + " already terminated."));
        }

        info.finished = true;
        info.error = error;
    }

    @Override
    public ConnectionsStatus getConnectionStatus(long secInTheLast) {
        long cutOffPoint = System.currentTimeMillis() - (secInTheLast * 1000);

        ConnectionsStatus res = new ConnectionsStatus();

        ConnectionsStatusInfo before = new ConnectionsStatusInfo();
        before.setSecInTheLast(secInTheLast);
        ConnectionsStatusInfo after = new ConnectionsStatusInfo();
        after.setSecInTheLast(0);

        res.setStatus(new ConnectionsStatusInfo[] {before, after});

        for (StatusInfo info: map.values()) {
            if (info.finished) {
                if (info.error == null) {
                    res.increaseConnectionClosed();;
                } else {
                    res.increaseConnectionClosedWithError();
                }
            } else {
                if (info.lastMessageDate >= cutOffPoint) {
                    before.increaseNumConnectionThatReceivedMessages();
                } else {
                    after.increaseNumConnectionThatReceivedMessages();
                }
            }
        }

        return res;
    }
}
