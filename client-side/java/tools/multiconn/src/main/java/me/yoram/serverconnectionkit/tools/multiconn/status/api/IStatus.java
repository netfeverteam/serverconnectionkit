package me.yoram.serverconnectionkit.tools.multiconn.status.api;

/**
 * Created by yoram on 21/10/16.
 */
public interface IStatus {
    void started(String name);
    void messageReceived(String name, String message);
    void finished(String name, Throwable error);

    ConnectionsStatus getConnectionStatus(long secInTheLast);
}
