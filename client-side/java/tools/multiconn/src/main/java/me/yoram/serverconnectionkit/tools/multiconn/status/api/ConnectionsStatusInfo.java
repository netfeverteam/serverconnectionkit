package me.yoram.serverconnectionkit.tools.multiconn.status.api;

/**
 * Created by yoram on 21/10/16.
 */
public class ConnectionsStatusInfo {
    private int numConnectionThatReceivedMessages = 0;
    private int numError = 0;
    private long secInTheLast = 0;

    public int getNumConnectionThatReceivedMessages() {
        return numConnectionThatReceivedMessages;
    }

    public void increaseNumConnectionThatReceivedMessages() {
        this.numConnectionThatReceivedMessages++;
    }

    public long getSecInTheLast() {
        return secInTheLast;
    }

    public void setSecInTheLast(long secInTheLast) {
        this.secInTheLast = secInTheLast;
    }

    public int getNumError() {
        return numError;
    }

    public void setNumError(int numError) {
        this.numError = numError;
    }
}
