package me.yoram.serverconnectionkit.tools.multiconn;

import me.yoram.serverconnectionkit.tools.multiconn.status.api.IStatus;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by yoram on 21/10/16.
 */
public class InputStreamRunnable implements Runnable {
    private final String connName;
    private final InputStream in;
    private final IStatus status;

    public InputStreamRunnable(final String connName, final InputStream in, final IStatus status) {
        super();

        this.connName = connName;
        this.in = in;
        this.status = status;
    }

    @Override
    public void run() {
        status.started(this.connName);
        try {
            boolean printed = false;
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while (true) {
                line = reader.readLine();

                if (line == null) {
                    //status.finished(connName, null);

                    Thread.sleep(1000);
                    //break;
                }

                if (line.startsWith("data: ")) {
                    status.messageReceived(connName, line);
                    int pos = line.indexOf(':');
                    int pos2 = line.indexOf('-');
                    String dataConnName = line.substring(pos + 1, pos2).trim();

                    if (!connName.equals(dataConnName)) {
                        throw new Exception(
                                String.format(
                                        "Names differed, expected [%s] but [%s] returned.", connName, dataConnName));
                    }

                    if (!printed) {
                        System.out.println(line);
                        printed = true;
                    }
                }

                Thread.sleep(1000);
            }
        } catch (Throwable t) {
            System.out.println(String.format("Error with connection %s. Connection closing ", connName));
            t.printStackTrace();
            status.finished(this.connName, t);
        }
    }
}
