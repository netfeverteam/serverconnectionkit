package me.yoram.serverconnectionkit.tools.multiconn;

import me.yoram.serverconnectionkit.tools.multiconn.info.ServerRunnable;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.IStatus;
import me.yoram.serverconnectionkit.tools.multiconn.status.impl.Status;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by yoram on 21/10/16.
 */
public class Launch {
    private static final IStatus STATUS = new Status();

    public static void main(String[] args) throws Exception {
        String page = args[0];
        System.out.println("Using page: " + page);

        int maxConn = Integer.parseInt(args[1]);
        System.out.println("Trying with " + maxConn + " connections");

        Thread serverThread = new Thread(new ServerRunnable(STATUS));
        serverThread.setDaemon(true);
        serverThread.start();
        Thread.sleep(1000);

        //int timeput = Integer.parseInt(args[2]);
        //System.out.println("Timeout " + maxConn + " connections");

        for (int i = 0; i < maxConn; i++) {
            System.out.println("Creating connection " + i);
            URL u = new URL(page + "?name=conn" + i);

            System.out.println("Opening " + i);
            HttpURLConnection conn = (HttpURLConnection)u.openConnection();

            InputStream in = conn.getInputStream();

            Thread t = new Thread(new InputStreamRunnable("conn" +i, in, STATUS));
            t.setDaemon(false);
            t.start();

            if (i % 256 == 0) {
                Thread.sleep(1000);
            }
        }

        while (true) {
            if (STATUS.getConnectionStatus(30).getConnectionOpened() == 0) {
                break;
            }

            Thread.sleep(1000);
        }
    }
}
