package me.yoram.serverconnectionkit.tools.multiconn.info;

import me.yoram.serverconnectionkit.tools.multiconn.Consts;
import me.yoram.serverconnectionkit.tools.multiconn.status.api.IStatus;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by yoram on 21/10/16.
 */
public class ServerRunnable implements Runnable {
    private final IStatus status;

    public ServerRunnable(final IStatus status) {
        super();

        this.status = status;
    }
    @Override
    public void run() {
        try {
            ServerSocket server = new ServerSocket(10000);

            try {
                Socket socket;

                while ((socket = server.accept()) != null) {
                    Thread t = new Thread(new ClientRunnable(socket, status));
                    t.setDaemon(false);
                    t.start();
                }
            } finally {
                server.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
            Runtime.getRuntime().halt(Consts.HALT_SERVER_ERROR);
        }
    }
}
