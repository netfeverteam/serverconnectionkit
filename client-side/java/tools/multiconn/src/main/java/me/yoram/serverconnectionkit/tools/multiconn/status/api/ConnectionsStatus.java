package me.yoram.serverconnectionkit.tools.multiconn.status.api;

import me.yoram.serverconnectionkit.tools.multiconn.status.impl.Status;

/**
 * Created by yoram on 21/10/16.
 */
public class ConnectionsStatus {
    private int connectionOpened = 0;
    private int connectionClosed = 0;
    private int connectionClosedWithError = 0;
    private ConnectionsStatusInfo[] status = null;

    public int getConnectionOpened() {
        return connectionOpened;
    }

    public void increaseConnectionOpened() {
        this.connectionOpened++;
    }

    public int getConnectionClosed() {
        return connectionClosed;
    }

    public void increaseConnectionClosed() {
        this.connectionClosed++;
    }

    public void setConnectionClosed(int connectionClosed) {
        this.connectionClosed = connectionClosed;
    }

    public int getConnectionClosedWithError() {
        return connectionClosedWithError;
    }

    public void increaseConnectionClosedWithError() {
        this.connectionClosedWithError++;
    }

    public void setConnectionClosedWithError(int connectionClosedWithError) {
        this.connectionClosedWithError = connectionClosedWithError;
    }

    public ConnectionsStatusInfo[] getStatus() {
        return status;
    }

    public void setStatus(ConnectionsStatusInfo[] status) {
        this.status = status;
    }
}
